package rogoz.patrick.lab7.Ex2;

import java.io.*;

public class EncryptDecrypt{


    String readFile() throws IOException {
        File file = new File("D:\\Facultate\\An II Sem II\\ISP\\rogoz-patrick-30121-isp-2020\\Lab7\\src\\rogoz\\patrick\\lab7\\Ex1\\text.txt");

        BufferedReader in = new BufferedReader(new FileReader(file));
        String s, s2 = new String();
        while ((s = in.readLine()) != null)
            s2 += s + "\n";
        return s2;
    }

    static String Enc(String srcString) {
        char[] result = new char[srcString.length()];
        for (int i = 0; i < result.length; i++) {
            result[i] = (char) (srcString.charAt(i) - 1);
        }
        String destString = new String(result);
        return destString;

    }

    static String Dec(String srcString) {
        char[] result = new char[srcString.length()];
        for (int i = 0; i < result.length; i++) {
            result[i] = (char) (srcString.charAt(i) + 1);
        }
        String destString = new String(result);
        return destString;
    }

    void writeFileEnc(String s) throws IOException {
        FileWriter out = null;
        char[] aux = s.toCharArray();
        out = new FileWriter("out.enc");
        for (int i = 0; i < aux.length; i++)
            out.write(aux[i]);

    }

    void writeFileDec(String s) throws IOException {
        FileWriter out = null;
        char[] aux = s.toCharArray();
        out = new FileWriter("out.dec");
        for (int i = 0; i < aux.length; i++)
            out.write(aux[i]);
    }
}