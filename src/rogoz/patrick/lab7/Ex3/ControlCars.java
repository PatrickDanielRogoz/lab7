package rogoz.patrick.lab7.Ex3;

import java.io.*;

public class ControlCars {

    Car createCar(String model, int price) {
        Car newCar = new Car(model, price);
        return newCar;
    }

    void save(String fileName) {
        try {
            ObjectOutputStream o =
                    new ObjectOutputStream(
                            new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Masina salvata in fisier");
        } catch (IOException e) {
            System.err.println("Masina nu poate fi scrisa in fisiser.");
            e.printStackTrace();
        }

    }

    static Car load(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(fileName));
        Car car = (Car) in.readObject();
        return car;
    }
}
}
