package rogoz.patrick.lab7.Ex3;

public class Car {
    String model;
    int price;

    public Car(String model, int price) {
        this.model = model;
        this.price = price;
    }
}